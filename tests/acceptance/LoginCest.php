<?php


class LoginCest
{
    public function validLogin(AcceptanceTester $I)
    {
        $I->am('user');
        $I->wantTo('log in successfully');
        $I->amOnPage('/login');
        $I->see('Username');
        $I->see('Password');
        $I->fillField(['name' => '_username'], 'some_user');
        $I->fillField(['name' => '_password'], 'some_pas55word');
        $I->click('_submit');
        $I->seeCurrentUrlEquals('/');
        $I->see('Profile');
        $I->see('Collections');
    }

    public function invalidLogin(AcceptanceTester $I)
    {
        $I->am('user');
        $I->wantTo('fail to log in');
        $I->amOnPage('/login');
        $I->see('Username');
        $I->see('Password');
        $I->fillField(['name' => '_username'], 'wrong_user');
        $I->fillField(['name' => '_password'], 'wrong_password');
        $I->click('_submit');
        $I->seeCurrentUrlEquals('/login');
        $I->see('Invalid credentials.');
    }
}
