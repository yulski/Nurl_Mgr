<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:36
 */

namespace Nurl_Mgr\AppBundle\Entity;


/**
 * Class Issue
 *
 * An issue that was raised by a user about a particular NURL.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class Issue
{
    /**
     * Identifier.
     *
     * @var integer
     */
    private $id;

    /**
     * The NURL the issue concerns.
     * @var \Nurl_Mgr\AppBundle\Entity\NURL
     */
    private $nurl;

    /**
     * Contact email provided by the user who reported the issue.
     * @var string
     */
    private $reporterEmail;

    /**
     * A description of the issue given by the user.
     * @var string
     */
    private $description;

    /**
     * A timestamp of when the issue was reported.
     * @var \DateTime
     */
    private $timeStamp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contact email for the reporter.
     *
     * @param string $reporterEmail The email address.
     *
     * @return Issue Returns self with updated email.
     */
    public function setReporterEmail($reporterEmail)
    {
        $this->reporterEmail = $reporterEmail;

        return $this;
    }

    /**
     * Get email address of reporter.
     *
     * @return string The reporter's email address.
     */
    public function getReporterEmail()
    {
        return $this->reporterEmail;
    }

    /**
     * Set description of the issue.
     *
     * @param string $description The description.
     *
     * @return Issue Returns self with updated description.
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description of the issue.
     *
     * @return string The description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set timeStamp the issue was reported at.
     *
     * @param \DateTime $timeStamp The timestamp.
     *
     * @return Issue Returns self with updated timestamp.
     */
    public function setTimeStamp($timeStamp)
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp that the issue was reported at.
     *
     * @return \DateTime The timestamp.
     */
    public function getTimeStamp()
    {
        return $this->timeStamp;
    }

    /**
     * Set NURL that the issue is about.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\NURL $nurl The NURL.
     *
     * @return Issue Returns self with updated nurl.
     */
    public function setNurl(\Nurl_Mgr\AppBundle\Entity\NURL $nurl = null)
    {
        $this->nurl = $nurl;

        return $this;
    }

    /**
     * Get NURL that the issue is about.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\NURL The NURL.
     */
    public function getNurl()
    {
        return $this->nurl;
    }
    /**
     * @var boolean
     */
    private $open = true;


    /**
     * Set open
     *
     * @param boolean $open
     *
     * @return Issue
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return boolean
     */
    public function getOpen()
    {
        return $this->open;
    }
}
