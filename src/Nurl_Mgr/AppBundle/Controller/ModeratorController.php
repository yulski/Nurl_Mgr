<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 05/04/2017
 * Time: 17:47
 */

namespace Nurl_Mgr\AppBundle\Controller;

use Nurl_Mgr\AppBundle\Entity\AccountFreeze;
use Nurl_Mgr\AppBundle\Entity\MessageIssues;
use Nurl_Mgr\AppBundle\Entity\NURLMessage;
use Nurl_Mgr\AppBundle\Entity\NURLMessages;
use Nurl_Mgr\AppBundle\Entity\User;
use Nurl_Mgr\AppBundle\Form\CreateUserFormType;
use Nurl_Mgr\AppBundle\Form\EditUserFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nurl_Mgr\AppBundle\Entity\NURL;
use Nurl_Mgr\AppBundle\Entity\Tag;
use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use Nurl_Mgr\AppBundle\Entity\TagVotes;

class ModeratorController extends  Controller implements ValidUserController
{
    public function indexAction(Request $request)
    {
        $nurlService = $this->get('app.nurl');
        $manager = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $nurlService->get($userId);
        $nurlService->pending();
        $pending = $nurlService->done();

        $nurlService->get($userId);
        $nurlService->reported();
        $reported = $nurlService->done();

        $moderatorId = $this->getUser()->getId();

        for($i=0; $i<count($pending); $i++) {
            $nurl = $pending[$i];
            $tags = $nurlService->getTags($nurl->getId(), $userId);
            $pending[$i] = ['nurl' => $nurl, 'tags' => $tags];
        }

        for($i=0; $i<count($reported); $i++) {
            $nurl = $reported[$i];
            $issues = $nurlService->getIssues($nurl->getId());
            $tags = $nurlService->getTags($nurl->getId(), $userId);
            $reported[$i] = ['nurl' => $nurl, 'tags' => $tags, 'issues' => $issues];
        }

        $userService = $this->get('app.user');

        $frozenUsers = $userService->getAllFrozenUsers();

        $freezeRepo = $manager->getRepository('AppBundle:AccountFreeze');

        $frozenAccounts = [];

        foreach($frozenUsers as $frozenUser) {
            $freeze = $freezeRepo->findBy(['user' => $frozenUser->getId(), 'moderator' => $moderatorId])[0];
            $frozenAccounts[] = ['user' => $frozenUser, 'freeze' => $freeze];
        }

        return $this->render('moderator/index.html.twig', [
            'pending' => $pending,
            'reported' => $reported,
            'frozen_accounts' => $frozenAccounts
        ]);
    }

    public function acceptNurlAction(Request $request)
    {
        $nurlId = $request->get('id');
        $nurlService = $this->get('app.nurl');
        $nurlService->accept($nurlId);
        return $this->redirectToRoute('moderator_page');
    }

    public function rejectNurlAction(Request $request)
    {
        $nurlId = $request->get('id');
        $nurlService = $this->get('app.nurl');
        $nurlService->delete($nurlId);
        return $this->redirectToRoute('moderator_page');
    }

    public function unfreezeFormAction(Request $request)
    {
        $nurlId = $request->get('id');
        return $this->render('moderator/reported_nurl_form.html.twig', [
            'title' => 'Unfreeze the NURL',
            'action' => $this->generateUrl('moderator_unfreeze_nurl', ['id' => $nurlId])
        ]);
    }

    public function deleteFormAction(Request $request)
    {
        $nurlId = $request->get('id');
        return $this->render('moderator/reported_nurl_form.html.twig', [
            'title' => 'Delete the NURL',
            'action' => $this->generateUrl('moderator_delete_nurl', ['id' => $nurlId])
        ]);
    }

    public function unfreezeAction(Request $request)
    {
        $nurlId = $request->get('id');
        $message = $request->get('message');

        $manager = $this->getDoctrine()->getManager();
        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        $nurlService = $this->get('app.nurl');
        $issues = $nurlService->getIssues($nurlId);

        $nurlMessage = new NURLMessage();
        $nurlMessage->setNurl($nurl);
        $nurlMessage->setMessage($message);
        $nurlMessage->setDeleted(false);
        foreach($issues as $issue) {
            $issue->setOpen(false);
            $nurlMessage->addIssue($issue);
            $messageIssue = new MessageIssues();
            $messageIssue->setMessage($nurlMessage);
            $messageIssue->setIssue($issue);
            $manager->persist($messageIssue);
            $manager->persist($issue);
        }
        $nurl->setIsFrozen(false);
        $nurlMessage->setTimeStamp(new \DateTime());

        $manager->persist($nurlMessage);
        $manager->persist($nurl);

        $manager->flush();

        return $this->redirectToRoute('moderator_page');
    }

    public function deleteAction(Request $request)
    {
        $nurlId = $request->get('id');
        $message = $request->get('message');

        $manager = $this->getDoctrine()->getManager();
        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        $nurlService = $this->get('app.nurl');
        $issues = $nurlService->getIssues($nurlId);

        $nurlMessage = new NURLMessage();
        $nurlMessage->setNurl($nurl);
        $nurlMessage->setMessage($message);
        $nurlMessage->setTimeStamp(new \DateTime());
        $manager->persist($nurlMessage);
        foreach($issues as $issue) {
            $issue->setOpen(false);
            $messageIssue = new MessageIssues();
            $messageIssue->setMessage($nurlMessage);
            $messageIssue->setIssue($issue);
            $manager->persist($messageIssue);
            $manager->persist($issue);
        }
        $nurl->setIsFrozen(false);
        $nurl->setIsRemoved(true);
        $manager->persist($nurl);

        $manager->flush();

        return $this->redirectToRoute('moderator_page');
    }

    public function freezeUserFormAction(Request $request)
    {
        $userId = $request->get('id');

        return $this->render('moderator/freeze_user.html.twig', ['id' => $userId]);
    }

    public function freezeUserAction(Request $request)
    {
        $userId = $request->get('id');

        $description = $request->get('description');

        $moderator = $this->getUser();

        $manager = $this->getDoctrine()->getManager();

        $user = $manager->getRepository('AppBundle:User')->find($userId);

        $freeze = new AccountFreeze();

        $freeze->setDescription($description);
        $freeze->setUser($user);
        $freeze->setModerator($moderator);

        $user->setIsFrozen(true);

        $manager->persist($user);

        $manager->persist($freeze);

        $manager->flush();

        $url = $this->generateUrl('user_public_profile', ['id' => $userId]);

        return $this->redirect($url);
    }

    public function unfreezeUserAction(Request $request)
    {
        $userId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $user = $manager->getRepository('AppBundle:User')->find($userId);

        $moderator = $this->getUser();

        $freezes = $manager->getRepository('AppBundle:AccountFreeze')->findBy(
            ['user' => $userId, 'moderator' => $moderator->getId()]);

        $user->setIsFrozen(false);

        $manager->persist($user);

        foreach($freezes as $freeze) {
            $manager->remove($freeze);
        }

        $manager->flush();

        $url = $this->generateUrl('user_public_profile', ['id' => $userId]);

        return $this->redirect($url);
    }

    public function issueArchiveAction(Request $request)
    {
        $issueService = $this->get('app.issue');

        $archive = $issueService->getMessageArchive();

        return $this->render('moderator/issue_archive.html.twig', [
            'archive' => $archive
        ]);
    }
}
