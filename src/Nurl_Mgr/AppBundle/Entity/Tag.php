<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:20
 */

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * Class Tag
 *
 * A tag for tagging NURLs.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class Tag
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * The value (text content).
     * @var string
     */
    private $value;

    /**
     * Number of votes
     * @var integer
     */
    private $votes = 0;

    /**
     * Is it accepted.
     * @var boolean
     */
    private $isAccepted;

    /**
     * The user who created the tag.
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $author;

    /**
     * Is the tag public.
     * @var boolean
     */
    private $isPublic;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value (text content).
     *
     * @param string $value The new value.
     *
     * @return Tag Return self with updated value.
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value (text content).
     *
     * @return string The value.
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the tag's vote score.
     *
     * @param integer $votes The vote score.
     *
     * @return Tag Returns self with updated votes.
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Get votes score.
     *
     * @return integer The votes.
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set accepted state.
     *
     * @param boolean $isAccepted Is the tag accepted.
     *
     * @return Tag Returns self with updated accepted state.
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get accepted state.
     *
     * @return boolean Is the tag accepted.
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set public visibility.
     *
     * @param boolean $isPublic Is the tag public.
     *
     * @return Tag Returns self with updated public visibility.
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get public visibility.
     *
     * @return boolean Is the tag public.
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set tag author.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $author The author.
     *
     * @return Tag Returns self with updated author.
     */
    public function setAuthor(\Nurl_Mgr\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the tag's author.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User The author.
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function incrementVotes(int $voteValue)
    {
        $this->votes += $voteValue;
    }

    public function decrementVotes(int $voteValue)
    {
        $this->votes -= $voteValue;
    }
}
