<?php
namespace Nurl_Mgr\AppBundle;
use Nurl_Mgr\AppBundle\FunctionalTester;

class NURLCest
{
    protected function login(FunctionalTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField(['name' => '_username'], 'some_user');
        $I->fillField(['name' => '_password'], 'some_pas55word');
        $I->click('_submit');
    }

    public function viewTest(FunctionalTester $I)
    {
        $I->am('user');
        $I->wantTo('view nurls');
        $I->amOnPage('/');
        $I->see('NURLs');
        $I->seeElement('.nurl');
    }

    /**
     * @before login
     */
    public function searchByText(FunctionalTester $I)
    {
        $I->am('logged in user');
        $I->wantTo('search for a nurl by text');
        $I->amOnPage('/nurl-search');
        $I->fillField(['id' => 'search'], 'title');
        $I->click('Submit');
        $I->seeCurrentUrlEquals('/');
        $I->seeElement('.nurl');
        $I->see('title');
    }
}
