<?php

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * MessageIssues
 */
class MessageIssues
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Nurl_Mgr\AppBundle\Entity\NURLMessage
     */
    private $message;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\Issue
     */
    private $issue;


    /**
     * Set message
     *
     * @param \Nurl_Mgr\AppBundle\Entity\NURLMessage $message
     *
     * @return MessageIssues
     */
    public function setMessage(\Nurl_Mgr\AppBundle\Entity\NURLMessage $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \Nurl_Mgr\AppBundle\Entity\NURLMessage
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set issue
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Issue $issue
     *
     * @return MessageIssues
     */
    public function setIssue(\Nurl_Mgr\AppBundle\Entity\Issue $issue = null)
    {
        $this->issue = $issue;

        return $this;
    }

    /**
     * Get issue
     *
     * @return \Nurl_Mgr\AppBundle\Entity\Issue
     */
    public function getIssue()
    {
        return $this->issue;
    }
}
