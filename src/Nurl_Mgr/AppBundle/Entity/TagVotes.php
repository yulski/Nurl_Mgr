<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:44
 */

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * Class TagVotes
 *
 * Association of tags and the users who voted on them.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class TagVotes
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ip_address;

    /**
     * The tag
     * @var \Nurl_Mgr\AppBundle\Entity\Tag
     */
    private $tag;

    /**
     * The user who voted on the tag
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $user;

    /**
     * The value of the vote
     * @var integer
     */
    private $vote;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     *
     * @return TagVotes
     */
    public function setIpAddress($ipAddress)
    {
        $this->ip_address = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * Set vote value.
     *
     * @param integer $vote The new vote value.
     *
     * @return TagVotes Self with updated vote.
     */
    public function setVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote value.
     *
     * @return integer The vote.
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Set tag.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Tag $tag The tag.
     *
     * @return TagVotes Self with updated tag.
     */
    public function setTag(\Nurl_Mgr\AppBundle\Entity\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\Tag The tag.
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set user.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $user The user.
     *
     * @return TagVotes Self with updated user.
     */
    public function setUser(\Nurl_Mgr\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User The user.
     */
    public function getUser()
    {
        return $this->user;
    }
}
