<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 17/02/2017
 * Time: 00:46
 */

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * Class TagNURLs
 *
 * Association of Tags and the NURLs they are tagging.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class TagNURLs
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * The tag.
     * @var \Nurl_Mgr\AppBundle\Entity\Tag
     */
    private $tag;

    /**
     * The nurl.
     * @var \Nurl_Mgr\AppBundle\Entity\NURL
     */
    private $nurl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Tag $tag The tag
     *
     * @return TagNURLs Returns self with updated tag
     */
    public function setTag(\Nurl_Mgr\AppBundle\Entity\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \Nurl_Mgr\AppBundle\Entity\Tag The tag
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set nurl
     *
     * @param \Nurl_Mgr\AppBundle\Entity\NURL $nurl The nurl
     *
     * @return TagNURLs Self with updated nurl.
     */
    public function setNurl(\Nurl_Mgr\AppBundle\Entity\NURL $nurl = null)
    {
        $this->nurl = $nurl;

        return $this;
    }

    /**
     * Get nurl
     *
     * @return \Nurl_Mgr\AppBundle\Entity\NURL The nurl
     */
    public function getNurl()
    {
        return $this->nurl;
    }
}
