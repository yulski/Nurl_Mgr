<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:47
 */

namespace Nurl_Mgr\AppBundle\Entity;


/**
 * Class CollectionAccess
 *
 * An association of collections and the users who have access to them.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class CollectionAccess
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\Collection A Collection which a particular user has access to.
     */
    private $collection;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\User A user who has access to the collection.
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the collection that access is being provided to.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Collection $collection The collection
     *
     * @return CollectionAccess Return self with updated collection.
     */
    public function setCollection(\Nurl_Mgr\AppBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get the collection that access is being provided to.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\Collection The collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set user who is given access to the collection.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $user The user
     *
     * @return CollectionAccess Return self with updated user
     */
    public function setUser(\Nurl_Mgr\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user who is being given access to the collection.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User The user
     */
    public function getUser()
    {
        return $this->user;
    }
}
