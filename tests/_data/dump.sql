-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2017 at 09:42 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nurl_mgr_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_freeze`
--

CREATE TABLE `account_freeze` (
  `id` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `moderator` int(11) DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `time_stamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE `collection` (
  `id` int(11) NOT NULL,
  `author` int(11) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`id`, `author`, `is_public`, `title`) VALUES
(1, 2, 0, 'My first collection');

-- --------------------------------------------------------

--
-- Table structure for table `collection_access`
--

CREATE TABLE `collection_access` (
  `id` int(11) NOT NULL,
  `collection` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collection_nurls`
--

CREATE TABLE `collection_nurls` (
  `id` int(11) NOT NULL,
  `nurl` int(11) DEFAULT NULL,
  `collection` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `collection_nurls`
--

INSERT INTO `collection_nurls` (`id`, `nurl`, `collection`) VALUES
(1, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE `issue` (
  `id` int(11) NOT NULL,
  `reporter_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `time_stamp` datetime NOT NULL,
  `open` tinyint(1) NOT NULL,
  `nurl` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `issue_nurl`
--

CREATE TABLE `issue_nurl` (
  `issue_id` int(11) NOT NULL,
  `nurl_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message_issues`
--

CREATE TABLE `message_issues` (
  `id` int(11) NOT NULL,
  `message` int(11) DEFAULT NULL,
  `issue` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nurl`
--

CREATE TABLE `nurl` (
  `id` int(11) NOT NULL,
  `author` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  `is_frozen` tinyint(1) NOT NULL,
  `is_accepted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `edited` datetime NOT NULL,
  `is_removed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nurl`
--

INSERT INTO `nurl` (`id`, `author`, `title`, `summary`, `content`, `is_public`, `is_frozen`, `is_accepted`, `created`, `edited`, `is_removed`) VALUES
(1, 1, 'This is a title', 'This is a brief summary of the NURL.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend est eget elementum tristique. Pellentesque ultricies ligula ornare nisi viverra rutrum. Vivamus tempor nulla at congue imperdiet. Mauris condimentum, felis ac pulvinar finibus, ex purus accumsan ante, nec cursus erat metus at est. Sed rutrum ex vitae tortor placerat, cursus mattis purus elementum. Mauris in dolor in odio finibus porttitor eu consectetur orci. Morbi dolor leo, commodo at sodales eget, posuere in massa. Fusce at dictum ex, at condimentum neque.', 1, 0, 1, '2017-03-02 13:00:00', '2017-03-30 16:49:57', 0),
(2, 1, 'My second NURL', 'Cras tempus fringilla nisi, id fringilla elit feugiat in. Cras ac nulla feugiat urna dignissim gravida. Nunc id dignissim felis. Integer rutrum nisl ut nisi accumsan porttitor. Nulla tortor metus, commodo porta arcu quis, vulputate pretium est.', 'Duis pulvinar pulvinar ipsum dapibus pretium. Praesent varius quam et libero porta, eget auctor odio semper. Nulla facilisi. Etiam vestibulum, augue sed mollis aliquet, est odio tempus arcu, sit amet viverra magna enim et tellus. Nunc placerat lectus in odio lobortis, ut porta diam placerat. Vivamus sagittis cursus nisl. Morbi vehicula non nisl fermentum elementum. Praesent a neque eget diam eleifend venenatis. Phasellus vitae orci lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque eu bibendum diam, et facilisis tortor. Mauris in enim sem. Nullam id libero diam. Proin tempor diam vel sapien consectetur mattis. Vivamus condimentum ipsum eu congue dapibus. Aenean feugiat lacus arcu, non sollicitudin leo hendrerit et.\n\nInteger ut euismod nunc. Pellentesque ut eros sem. Nulla diam quam, sagittis in neque eu, elementum dictum est. Integer vel mi consequat, iaculis tellus lobortis, ultricies erat. Nunc vitae felis libero. Maecenas porta hendrerit orci ac tincidunt. Proin vulputate metus eros, a pharetra nunc interdum at. Ut quis maximus nibh. Nunc eu luctus diam. Etiam a odio ac nunc mattis ullamcorper vel quis lectus. Curabitur nisl tellus, consectetur ut massa at, vehicula gravida nisi. Integer bibendum gravida urna non efficitur. Vivamus sed sapien non orci auctor efficitur pulvinar sit amet nibh.', 0, 0, 1, '2017-04-07 00:00:00', '2017-03-07 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nurl_access`
--

CREATE TABLE `nurl_access` (
  `id` int(11) NOT NULL,
  `nurl` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nurl_message`
--

CREATE TABLE `nurl_message` (
  `id` int(11) NOT NULL,
  `nurl` int(11) DEFAULT NULL,
  `message` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `time_stamp` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `author` int(11) DEFAULT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `votes` int(11) NOT NULL,
  `is_accepted` tinyint(1) NOT NULL,
  `is_public` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `author`, `value`, `votes`, `is_accepted`, `is_public`) VALUES
(1, 1, 'example', 0, 1, 1),
(2, 1, 'another', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tag_nurls`
--

CREATE TABLE `tag_nurls` (
  `id` int(11) NOT NULL,
  `tag` int(11) DEFAULT NULL,
  `nurl` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tag_nurls`
--

INSERT INTO `tag_nurls` (`id`, `tag`, `nurl`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_frozen` tinyint(1) NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated` datetime NOT NULL,
  `private` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `is_frozen`, `username_canonical`, `email_canonical`, `enabled`, `salt`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `profile_picture`, `updated`, `private`) VALUES
(1, 'some_user', 'some_email@yahoo.com', '$2y$13$RJh5.cPoyg/WwZ37GeV3OOx.4kFZ4bmuCJyHsmCrw1tS/pZGWJvcy', 0, 'some_user', 'some_email@yahoo.com', 1, NULL, '2017-04-16 00:50:12', NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', '', '2017-04-03 22:39:27', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_freeze`
--
ALTER TABLE `account_freeze`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_61A53A698D93D649` (`user`),
  ADD KEY `IDX_61A53A696A30B268` (`moderator`);

--
-- Indexes for table `collection`
--
ALTER TABLE `collection`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FC4D6532BDAFD8C8` (`author`);

--
-- Indexes for table `collection_access`
--
ALTER TABLE `collection_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F44CD3A2FC4D6532` (`collection`),
  ADD KEY `IDX_F44CD3A28D93D649` (`user`);

--
-- Indexes for table `collection_nurls`
--
ALTER TABLE `collection_nurls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AC086EDFF18AC390` (`nurl`),
  ADD KEY `IDX_AC086EDFFC4D6532` (`collection`);

--
-- Indexes for table `issue`
--
ALTER TABLE `issue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_12AD233EF18AC390` (`nurl`);

--
-- Indexes for table `issue_nurl`
--
ALTER TABLE `issue_nurl`
  ADD PRIMARY KEY (`issue_id`,`nurl_id`),
  ADD KEY `IDX_A96D0B5C5E7AA58C` (`issue_id`),
  ADD KEY `IDX_A96D0B5C76E89382` (`nurl_id`);

--
-- Indexes for table `message_issues`
--
ALTER TABLE `message_issues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_ED5AE28CB6BD307F` (`message`),
  ADD KEY `IDX_ED5AE28C12AD233E` (`issue`);

--
-- Indexes for table `nurl`
--
ALTER TABLE `nurl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F18AC390BDAFD8C8` (`author`);

--
-- Indexes for table `nurl_access`
--
ALTER TABLE `nurl_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_42620F00F18AC390` (`nurl`),
  ADD KEY `IDX_42620F008D93D649` (`user`);

--
-- Indexes for table `nurl_message`
--
ALTER TABLE `nurl_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DAFFAEB6F18AC390` (`nurl`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_389B783BDAFD8C8` (`author`);

--
-- Indexes for table `tag_nurls`
--
ALTER TABLE `tag_nurls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C91CCF8A389B783` (`tag`),
  ADD KEY `IDX_C91CCF8AF18AC390` (`nurl`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_freeze`
--
ALTER TABLE `account_freeze`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `collection`
--
ALTER TABLE `collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `collection_access`
--
ALTER TABLE `collection_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `collection_nurls`
--
ALTER TABLE `collection_nurls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `issue`
--
ALTER TABLE `issue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `message_issues`
--
ALTER TABLE `message_issues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nurl`
--
ALTER TABLE `nurl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `nurl_access`
--
ALTER TABLE `nurl_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nurl_message`
--
ALTER TABLE `nurl_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tag_nurls`
--
ALTER TABLE `tag_nurls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
