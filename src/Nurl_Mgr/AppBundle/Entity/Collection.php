<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:23
 */

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * Class Collection
 *
 * Collection created by a user. Each collection has a certain number of NURLs associated with it.
 * Collections can be public or private to the user who created them. Private collections can also be shared
 * with individual users.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class Collection
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * The user who created the collection.
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $author;

    /**
     * Visibility of the collection (public or private).
     * @var boolean
     */
    private $isPublic;

    /**
     * @var string
     */
    private $title;


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Collection
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get collection id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the collection visibility.
     *
     * @param boolean $isPublic The new value for the collection's visibility.
     *
     * @return Collection Return self with updated visibility value.
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get the collection visibility.
     *
     * @return boolean True if collection is public and false if private.
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set the owner of the collection.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $author The user who should be made owner of the collection.
     *
     * @return Collection Return self with updated author.
     */
    public function setAuthor(\Nurl_Mgr\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the author of the collection.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User A User who is the collection's author and owner.
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
