(function() {
    var upvote = $('.up-vote');
    var downvote = $('.down-vote');
    var del = $('#tag-delete');

    $(upvote).click(function(evt) {
        var form = $(evt.target).parent();
        $(form).submit();
    });

    $(downvote).click(function(evt) {
        var form = $(evt.target).parent();
        $(form).submit();
    });

    $(del).click(function(evt) {
        var elem = $(del).parent();
        var form = $(elem).parent();
        $(form).submit();
    })
})();
