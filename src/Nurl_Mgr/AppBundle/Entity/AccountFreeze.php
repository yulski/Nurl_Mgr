<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 11/04/2017
 * Time: 15:02
 */

namespace Nurl_Mgr\AppBundle\Entity;


class AccountFreeze
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $moderator;

    public function __construct()
    {
        $this->timeStamp = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AccountFreeze
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set user
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $user
     *
     * @return AccountFreeze
     */
    public function setUser(\Nurl_Mgr\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set moderator
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $moderator
     *
     * @return AccountFreeze
     */
    public function setModerator(\Nurl_Mgr\AppBundle\Entity\User $moderator = null)
    {
        $this->moderator = $moderator;

        return $this;
    }

    /**
     * Get moderator
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User
     */
    public function getModerator()
    {
        return $this->moderator;
    }
    /**
     * @var \DateTime
     */
    private $timeStamp;


    /**
     * Set timeStamp
     *
     * @param \DateTime $timeStamp
     *
     * @return AccountFreeze
     */
    public function setTimeStamp(\DateTime $timeStamp)
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp
     *
     * @return \DateTime
     */
    public function getTimeStamp()
    {
        return $this->timeStamp;
    }
}
