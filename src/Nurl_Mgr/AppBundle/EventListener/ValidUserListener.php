<?php

namespace Nurl_Mgr\AppBundle\EventListener;

use Nurl_Mgr\AppBundle\Controller\ValidUserController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Nurl_Mgr\AppBundle\Entity\User;

class ValidUserListener
{
    private $container;
    private $router;

    public function __construct(ContainerInterface $container, Router $router)
    {
        $this->container = $container;
        $this->router = $router;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getRouter()
    {
        return $this->router;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if(!is_array($controller)) {
            return;
        }

        if($controller[0] instanceof ValidUserController) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            echo $user;
            if($user instanceof User && $user->isFrozen()) {
                $redirectUrl = $this->router->generate('user_account_frozen');
                $event->setController(function() use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
            }
        }
    }
}
