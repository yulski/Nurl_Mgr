<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:17
 */

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * Class NURL
 *
 * A NURL instance.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class NURL
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * The NURL title.
     * @var string
     */
    private $title;

    /**
     * A summary of the NURL.
     * @var string
     */
    private $summary;

    /**
     * The content/body of the NURL.
     * @var string
     */
    private $content;

    /**
     * The NURL's author.
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $author;

    /**
     * Visibility of the NURL (is it public or private).
     * @var boolean
     */
    private $isPublic;

    /**
     * Frozen status of the NURL. The NURL can be frozen by moderator or not.
     * @var boolean
     */
    private $isFrozen = false;

    /**
     * Accepted status of the NURL. NURL can be accepted by moderator or not.
     * @var boolean
     */
    private $isAccepted = false;

    /**
     * The date when the NURL was created.
     * @var \DateTime
     */
    private $created;

    /**
     * The date when the NURL was last edited.
     * @var \DateTime
     */
    private $edited;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title of the NURL.
     *
     * @param string $title The title
     *
     * @return NURL Returns self with updated title.
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of the NURL.
     *
     * @return string The title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set summary of the NURL.
     *
     * @param string $summary The summary
     *
     * @return NURL Returns self with updated summary.
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary of the NURL.
     *
     * @return string The summary
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set content of the NURL.
     *
     * @param string $content The content.
     *
     * @return NURL Returns self with updated content.
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content of the NURL.
     *
     * @return string The content.
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set NURL visibility (public or private).
     *
     * @param boolean $isPublic Visibility
     *
     * @return NURL Return self with updated visibility.
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get NURL visibility.
     *
     * @return boolean The NURL's visibility. True if public, false if private.
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set frozen state of the NURL.
     *
     * @param boolean $isFrozen The frozen state.
     *
     * @return NURL Return self with updated frozen state.
     */
    public function setIsFrozen($isFrozen)
    {
        $this->isFrozen = $isFrozen;

        return $this;
    }

    /**
     * Get frozen state.
     *
     * @return boolean The frozen state. True if frozen, false if not.
     */
    public function getIsFrozen()
    {
        return $this->isFrozen;
    }

    /**
     * Set NURL's accepted state.
     *
     * @param boolean $isAccepted The accepted state
     *
     * @return NURL Returns self with updated accepted state.
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get NURL updated state.
     *
     * @return boolean The NURL's accepted state. True if NURL is accepted, false otherwise.
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set the NURL's creation date.
     *
     * @param \DateTime $created The creation date.
     *
     * @return NURL Returns self with updated creation date.
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get the date the NURL was created.
     *
     * @return \DateTime The creation date.
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the date when the NURL was last edited.
     *
     * @param \DateTime $edited The edit date.
     *
     * @return NURL Returns self with updated edit date.
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;

        return $this;
    }

    /**
     * Get the date when the NURL was last edited.
     *
     * @return \DateTime The last edit date.
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * Set the NURL's author.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $author The author.
     *
     * @return NURL Returns self with updated author.
     */
    public function setAuthor(\Nurl_Mgr\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the user who created the NURL.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User The author.
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function equals(NURL $otherEntity)
    {
        return $otherEntity->getId() === $this->id;
    }
    /**
     * @var boolean
     */
    private $isRemoved = false;


    /**
     * Set isRemoved
     *
     * @param boolean $isRemoved
     *
     * @return NURL
     */
    public function setIsRemoved($isRemoved)
    {
        $this->isRemoved = $isRemoved;

        return $this;
    }

    /**
     * Get isRemoved
     *
     * @return boolean
     */
    public function getIsRemoved()
    {
        return $this->isRemoved;
    }
}
