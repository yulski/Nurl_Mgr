[![Build Status](https://travis-ci.com/julek14/Nurl_Mgr.svg?token=xreXjtTKfpJG4gzUDcZs&branch=master)](https://travis-ci.com/julek14/Nurl_Mgr)

## NURL Manager
*(notes + URL)*

An online system to organise and share useful links and notes.

### Use cases
##### role 'public' (no login required)
* can see most recently added public NURLs
	NURL = NURL content + tags + text title + text summary
	\+ id (Link to public profile) of author
	\+ list of any issues (freezing + comments)

* can see list of tags

* can see list of proposed new tags
* up/down vote list of proposed new tags (anonymous users votes count as 1 vote)
* can propose new candidate tag

* can submit suggestions for new NURL (goes into 'pending' list for site moderators to accept / reject)
	NURL content + tags + text title + text summary
	
* can report a public NURL to moderator (for unacceptable content) (goes into 'frozen' list for moderator and is no longer public)
	NURL id + timestamp + text description of why unacceptable + email (optional) of reporter

##### role 'user' 

+ can view / search NURLs in collections
	+/- own collections
	+/- shared collections from other users
	+/- all public NURLs

can search by:
* matching one or more tags 
* free text content search
* by date range (created / last edit)
	
* can CRUD personal tags

* can CRUD own NURLs
	* can share a NURL with other users
	* can change a NURL from private (default) to public 
		NOTE - cannot change back once public
		once public NURL remains in DB even if user is deleted 

* can CRUD own collections
	* can share a collection with other users

* up/down vote list of proposed new tags (registered users votes count as 5 votes)

* delete user account

* can update profile
	EXTRA marks - upload of profile picture
	* can make profile public / private (user name is always public for public NURLs)

##### role 'admin'
* can CRUD user and admin accounts

##### role 'moderator'
* can review 'pending' and 'reported' NURLS
	* new NURL - accept / reject

	* reported public NURL - delete (if moderator agrees content unacceptable) / unfree (if moderator disagrees) - in both cases moderator leaves a timestamped text comment to justify decision

* can freeze user accounts
	user id + timestamp + text description of why frozen + user id of moderator
