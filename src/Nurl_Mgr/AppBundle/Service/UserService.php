<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 02/04/2017
 * Time: 22:39
 */

namespace Nurl_Mgr\AppBundle\Service;


use Doctrine\ORM\EntityManager;

class UserService
{
    protected $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function delete($userId)
    {
        $user = $this->manager->getRepository('AppBundle:User')->find($userId);
        $this->manager->remove($user);
        $this->manager->flush();
    }

    public function getAllFrozenUsers()
    {
        return $this->manager->getRepository('AppBundle:User')->findBy(['isFrozen' => true]);
    }
}