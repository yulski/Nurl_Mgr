<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:46
 */

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * Class CollectionNURLs
 *
 * Association of NURLs and the collections they belong to.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class CollectionNURLs
{
    /**
     * Identifier.
     * @var integer
     */
    private $id;

    /**
     * The NURL which belongs to some collection.
     * @var \Nurl_Mgr\AppBundle\Entity\NURL
     */
    private $nurl;

    /**
     * The collection the NURL belongs to.
     * @var \Nurl_Mgr\AppBundle\Entity\Collection
     */
    private $collection;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set NURL that belongs to the collection.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\NURL $nurl The NURL.
     *
     * @return CollectionNURLs Returns self with updated nurl.
     */
    public function setNurl(\Nurl_Mgr\AppBundle\Entity\NURL $nurl = null)
    {
        $this->nurl = $nurl;

        return $this;
    }

    /**
     * Get NURL that belongs to collection.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\NURL The NURL
     */
    public function getNurl()
    {
        return $this->nurl;
    }

    /**
     * Set collection that the NURL belongs to.
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Collection $collection The collection
     *
     * @return CollectionNURLs Returns self with updated collection
     */
    public function setCollection(\Nurl_Mgr\AppBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection that the NURL belongs to.
     *
     * @return \Nurl_Mgr\AppBundle\Entity\Collection The collection
     */
    public function getCollection()
    {
        return $this->collection;
    }
}
