<?php

namespace Nurl_Mgr\AppBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\ProfileController as BaseController;

class ProfileController extends BaseController implements ValidUserController
{

    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $nurlService = $this->get('app.nurl');

        $tagService = $this->get('app.tag');

        $userId = $user->getId();

        $nurlService->get($userId);
        $nurlService->own();
        $ownNurls = $nurlService->done();

        for($i=0; $i<count($ownNurls); $i++) {
            $nurl = $ownNurls[$i];
            $tags = $nurlService->getTags($nurl->getId(), $userId);
            $ownNurls[$i] = ['nurl' => $nurl, 'tags' => $tags];
        }

        usort($ownNurls, function($a, $b) {
            $d1 = $a['nurl']->getCreated();
            $d2 = $b['nurl']->getCreated();
            if($d1 == $d2) {
                return 0;
            }
            return ($d1 > $d2) ? -1 : +1;
        });

        $ownTags = $tagService->getAllOwnTags($userId);

        return $this->render('@FOSUser/Profile/show.html.twig', [
            'user' => $user,
            'nurls' => $ownNurls,
            'tags' => $ownTags
        ]);
    }
}