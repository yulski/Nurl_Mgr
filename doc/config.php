<?php

return new Sami\Sami('src', array(
    'title' => 'Nurl_Mgr API',
    'build_dir' => 'doc/build',
    'cache_dir' => 'doc/cache',
    'remote_repository' => new \Sami\RemoteRepository\GitHubRemoteRepository('julek14/Nurl_Mgr', '.')
));
