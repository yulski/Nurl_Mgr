<?php

namespace Nurl_Mgr\AppBundle\Service;

use Nurl_Mgr\AppBundle\Entity\NURLAccess;
use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use Nurl_Mgr\AppBundle\Entity\User;
use Doctrine\DBAL\Query\QueryBuilder;
use \Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Nurl_Mgr\AppBundle\Entity\NURL;
use \DateTime;

class NURLService
{
    private $buffer = [];
    private $userId;
    protected $manager;

    public function __construct(EntityManager $manager)
    {
        $this->reset();
        $this->manager = $manager;
    }

    protected function reset()
    {
        $this->buffer = null;
        $this->userId = -1;
    }

    public function get($userId)
    {
        $this->userId = $userId;
    }

    public function public()
    {
        $conditions = ['isPublic' => true, 'isAccepted' => true, 'isFrozen' => false, 'isRemoved' => false];
        $nurls = $this->manager->getRepository('AppBundle:NURL')->findBy($conditions);
        $this->buffer[] = $nurls;
    }

    public function private()
    {
        $conditions = ['isPublic' => false, 'isAccepted' => true, 'isFrozen' => false,
            'author' => $this->userId, 'isRemoved' => false];
        $nurls = $this->manager->getRepository('AppBundle:NURL')->findBy($conditions);
        $this->buffer[] = $nurls;
    }

    public function shared()
    {
        $nurlAccessRepo = $this->manager->getRepository('AppBundle:NURLAccess');

        $userAccess = $nurlAccessRepo->findBy(['user' => $this->userId]);

        $nurls = [];

        foreach($userAccess as $access) {
            $nurls[] = $access->getNurl();
        }

        $this->buffer[] = $nurls;
    }

    public function own()
    {
        $conditions = ['author' => $this->userId, 'isAccepted' => true, 'isFrozen' => false, 'isRemoved' => false];
        $nurls = $this->manager->getRepository('AppBundle:NURL')->findBy($conditions);
        $this->buffer[] = $nurls;
    }

    public function ownPublic()
    {
        $conditions = ['author' => $this->userId, 'isAccepted' => true, 'isFrozen' => false, 'isRemoved' => false, 'isPublic' => true];
        $nurls = $this->manager->getRepository('AppBundle:NURL')->findBy($conditions);
        $this->buffer[] = $nurls;
    }

    public function pending()
    {
        $nurlAccessRepo = $this->manager->getRepository('AppBundle:NURL');
        $nurls = $nurlAccessRepo->findBy(['isAccepted' => false]);
        $this->buffer[] = $nurls;
    }

    public function reported()
    {
        $nurlAccessRepo = $this->manager->getRepository('AppBundle:NURL');
        $nurls = $nurlAccessRepo->findBy(['isFrozen' => true]);
        $this->buffer[] = $nurls;
    }

    public function all()
    {
        $this->private();
        $this->public();
        $this->shared();
    }

    public function done()
    {
        $result = [];
        foreach($this->buffer as $arr) {
            $result = array_merge($result, $arr);
        }
        $this->reset();
        return $result;
    }

    public function search($text, $dates)
    {
        for($i=0; $i<count($this->buffer); $i++) {
            $curr = $this->buffer[$i];
            $this->buffer[$i] = $this->filter($curr, $text, $dates);
        }
    }

//    public function search($text)
//    {
//        $this->builder->andWhere($this->builder->expr()->orX(
//            $this->builder->expr()->like('n.title', ':text'),
//            $this->builder->expr()->like('n.summary', ':text'),
//            $this->builder->expr()->like('n.content', ':text')
//        ))
//        ->setParameter(':text', '%' . $text . '%');
//    }
//
//    public function dates($createdFrom, $createdTo, $editedFrom, $editedTo)
//    {
//        if($createdFrom) {
//            $date = new DateTime($createdFrom);
//            $date->setTime(0, 0);
//            $this->createdFrom($date);
//        }
//        if($createdTo) {
//            $date = new DateTime($createdTo);
//            $date->setTime(23, 59, 59);
//            $this->createdTo($date);
//        }
//        if($editedFrom) {
//            $date = new DateTime($editedFrom);
//            $date->setTime(0, 0);
//            $this->editedFrom($date);
//        }
//        if($editedTo) {
//            $date = new DateTime($editedTo);
//            $date->setTime(23, 59, 59);
//            $this->editedTo($date);
//        }
//    }
//
//    public function createdFrom(DateTime $dateTime)
//    {
//        $this->builder->andWhere('n.created > :createdFrom')
//            ->setParameter('createdFrom', $dateTime);
//    }
//
//    public function createdTo(DateTime $dateTime)
//    {
//        $this->builder->andWhere('n.created < :createdTo')
//            ->setParameter('createdTo', $dateTime);
//    }
//
//    public function editedFrom(DateTime $dateTime)
//    {
//        $this->builder->andWhere('n.edited < :editedFrom')
//            ->setParameter('editedFrom', $dateTime);
//    }
//
//    public function editedTo(DateTime $dateTime)
//    {
//        $this->builder->andWhere('n.edited < :editedTo')
//            ->setParameter('editedTo', $dateTime);
//    }

    public function filter(array $nurls, $text = '', $dates = [])
    {
        return array_filter($nurls, function(NURL $nurl) use ($text, $dates) {
            $textValid = true;
            if($text) {
                $titleIndex = (strpos($nurl->getTitle(), $text) !== false);
                $summaryIndex = (strpos($nurl->getSummary(), $text) !== false);
                $contentIndex = (strpos($nurl->getContent(), $text) !== false);
                $textValid = ($titleIndex || $summaryIndex || $contentIndex);
            }
            $dateValid = true;
            if(count($dates) > 0) {
                $nurlCreated = $nurl->getCreated();
                $nurlEdited = $nurl->getEdited();
                if($dates['createdFrom']) {
                    $date = new DateTime($dates['createdFrom']);
                    $dateValid = $nurlCreated > $date;
                }
                if($dates['createdTo']) {
                    $date = new DateTime($dates['createdTo']);
                    $dateValid = $nurlCreated < $date;
                }
                if($dates['editedFrom']) {
                    $date = new DateTime($dates['editedFrom']);
                    $dateValid = $nurlEdited > $date;
                }
                if($dates['editedTo']) {
                    $date = new DateTime($dates['editedTo']);
                    $dateValid = $nurlEdited < $date;
                }
            }
            return $textValid && $dateValid;
        });
    }

    public function makePublic(NURL $nurl)
    {
        $nurl->setIsPublic(true);
        $this->manager->persist($nurl);
        $this->manager->flush();
    }

    public function create(array $nurlData)
    {
        $nurl = new NURL();
        $nurl->setTitle($nurlData['title']);
        $nurl->setSummary($nurlData['summary']);
        $nurl->setContent($nurlData['content']);
        $nurl->setIsPublic($nurlData['public']);
        $nurl->setCreated($nurlData['created']);
        $nurl->setEdited($nurlData['edited']);
        if(array_key_exists('author', $nurlData)) {
            $nurl->setAuthor($nurlData['author']);
            $nurl->setIsAccepted(true);
        }

        $this->manager->persist($nurl);
        $this->manager->flush();

        if(array_key_exists('tags', $nurlData)) {
            foreach($nurlData['tags'] as $tag) {
                $tagNurl = new TagNURLs();
                $tagNurl->setTag($tag);
                $tagNurl->setNurl($nurl);

                $this->manager->persist($tagNurl);
            }
            $this->manager->flush();
        }
    }

    public function freeze($nurlId)
    {
        $nurl = $this->manager->getRepository('AppBundle:NURL')->find($nurlId);
        $nurl->setIsFrozen(true);
        $this->manager->persist($nurl);
        $this->manager->flush();
    }

    public function unfreeze($nurlId)
    {
        $nurl = $this->manager->getRepository('AppBundle:NURL')->find($nurlId);
        $nurl->setIsFrozen(false);
        $nurl->setIsPublic(true);
        $this->manager->persist($nurl);
        $this->manager->flush();
    }

    public function getTags($nurlId, $userId)
    {
        $user = null;
        if($userId) {
            $user = $this->manager->getRepository('AppBundle:User')->find($userId);
        }
        $tagNurlRepo = $this->manager->getRepository('AppBundle:TagNURLs');
        $tagNurls = $tagNurlRepo->findBy(['nurl' => $nurlId]);
        $tags = [];
        foreach($tagNurls as $tagNurl) {
            $currTag = $tagNurl->getTag();
            if($currTag) {
                if ($currTag->getIsPublic() === true) {
                    $tags[] = $currTag;
                } else if ($currTag->getIsPublic() === false && $user && $currTag->getAuthor()->getId() ===
                    $user->getId()
                ) {
                    $tags[] = $currTag;
                }
            }
        }
        return $tags;
    }

    public function delete($nurlId)
    {
        $nurl = $this->manager->getRepository('AppBundle:NURL')->find($nurlId);
        $this->manager->remove($nurl);
        $this->manager->flush();
    }

    public function edit(NURL $nurl, array $info)
    {
        $now = new DateTime();
        $nurl->setTitle($info['title']);
        $nurl->setSummary($info['summary']);
        $nurl->setContent($info['content']);
        $nurl->setEdited($now);
        $this->manager->persist($nurl);
        $this->manager->flush();
    }

    public function share(NURL $nurl, User $user)
    {
        $nurlAccessRepo = $this->manager->getRepository('AppBundle:NURLAccess');

        $currAccess = $nurlAccessRepo->findBy(['nurl' => $nurl->getId(), 'user' => $user->getId()]);

        if(count($currAccess) > 0) {
            return;
        }

        $access = new NURLAccess();
        $access->setUser($user);
        $access->setNurl($nurl);
        $this->manager->persist($access);
        $this->manager->flush();
    }

    public function accept($nurlId)
    {
        $nurl = $this->manager->getRepository('AppBundle:NURL')->find($nurlId);
        $nurl->setIsAccepted(true);
        $this->manager->persist($nurl);
        $this->manager->flush();
    }

    public function getIssues($nurlId)
    {
        $nurl = $this->manager->getRepository('AppBundle:NURL')->find($nurlId);
        $issueRepo = $this->manager->getRepository('AppBundle:Issue');
        $issues = $issueRepo->findBy(['nurl' => $nurl, 'open' => true]);
        return $issues;
    }
}
