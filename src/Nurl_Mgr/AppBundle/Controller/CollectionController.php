<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 28/03/2017
 * Time: 11:39
 */

namespace Nurl_Mgr\AppBundle\Controller;

use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Nurl_Mgr\AppBundle\Entity\NURL;
use Nurl_Mgr\AppBundle\Entity\Tag;
use Symfony\Component\HttpFoundation\Response;

class CollectionController extends Controller implements ValidUserController
{
    public function getAction(Request $request)
    {
        $public = $request->get('public');
        $own = $request->Get('own');
        $shared = $request->get('shared');
        $search = $request->get('search');
        $searchTags = $request->get('tags');
        $createdFrom = $request->get('created-from');
        $createdTo = $request->get('created-to');
        $editedFrom = $request->get('edited-from');
        $editedTo = $request->get('edited-to');

        $user = $this->getUser();

        if(empty($public) && empty($own) && empty($shared)) {
            $public = true;
            $own = true;
            $shared = true;
        }

        $userId = $user->getId();

        $manager = $this->getDoctrine()->getManager();

        $collectionService = $this->get('app.collection');

        $nurlService = $this->get('app.nurl');

        $collectionService->get($userId);

        if($public) {
            $collectionService->public();
        }
        if($own) {
            $collectionService->own();
        }
        if($shared) {
            $collectionService->shared();
        }

        $allCollections = $collectionService->done();

        $collections = [];

        $dates = [
            'createdFrom' => $createdFrom,
            'createdTo' => $createdTo,
            'editedFrom' => $editedFrom,
            'editedTo' => $editedTo
        ];

        $performSearch = $search || ($createdFrom || $createdTo || $editedFrom || $editedTo) || (!empty($searchTags));

        $tagNurlRepo = $manager->getRepository('AppBundle:TagNURLs');

        foreach($allCollections as $collection) {
            $collectionId = $collection->getId();
            $nurls = [];
            $collectionNurls = $collectionService->getNurls($collectionId);
            if($performSearch) {
                $collectionNurls = $nurlService->filter($collectionNurls, $search, $dates);
                if(!empty($searchTags[0])) {
                    $collectionNurls = array_filter($collectionNurls, function (NURL $nurl) use
                    ($tagNurlRepo, $searchTags) {
                        $hasTags = true;
                        foreach ($searchTags as $tagId) {
                            $hasTags = $hasTags &&
                                $tagNurlRepo->findBy(['nurl' => $nurl->getId(), 'tag' => $tagId]);
                        }
                        return $hasTags;
                    });
                }
                if (count($collectionNurls) === 0) {
                    continue;
                }
            }
            foreach($collectionNurls as $nurl) {
                $nurlId = $nurl->getId();
                $tags = $nurlService->getTags($nurlId, $userId);
                $nurls[] = ['nurl' => $nurl, 'tags' => $tags];
            }
            $collections[] = ['collection' => $collection, 'nurls' => $nurls];
        }

        $args = [
            'collections' => $collections,
            'active_tab' => 'collections',
            'public' => $public,
            'own' => $own,
            'shared' => $shared
        ];

        if($performSearch) {
            $args['search'] = [
                'tags' => $searchTags,
                'text' => $search
            ];
        } else {
            $args['search'] = false;
        }

        return $this->render('default/collections.html.twig', $args);
    }

    public function searchFormAction(Request $request)
    {
        $tagService = $this->get('app.tag');

        $allTags = $tagService->getAcceptedTags();

        if($user = $this->getUser()) {
            $personalTags = $tagService->getPersonalTags($user->getId());
            $allTags = array_merge($allTags, $personalTags);
        }

        $title = 'Search for a NURL in collections';
        $actionPath = $this->get('router')->getRouteCollection()->get('homepage_collections')->getPath();
        return $this->render('default/nurl_search.html.twig', [
            'title' => $title,
            'action' => $actionPath,
            'tags' => $allTags,
            'privateName' => 'own',
            'sharedName' => 'shared',
            'publicName' => 'public',
            'searchName' => 'search'
        ]);
    }

    public function deleteAction(Request $request)
    {
        $collectionId = (int) $request->get('id');

        $user = $this->getUser();

        $manager = $this->getDoctrine()->getManager();

        $collection = $manager->getRepository('AppBundle:Collection')->find($collectionId);

        if($collection->getAuthor()->getId() !== $user->getId()) {
            return $this->redirectToRoute('homepage_collections');
        }

        $this->get('app.collection')->delete($collection);

        return $this->redirectToRoute('homepage_collections');
    }

    public function shareFormAction(Request $request)
    {
        $collectionId = (int) $request->get('id');

        $actionPath = $this->generateUrl('collection_share', [
            'id' => $collectionId
        ]);

        return $this->render('default/entity_share.html.twig', [
            'entity' => 'Collection',
            'action' => $actionPath
        ]);
    }

    public function shareAction(Request $request)
    {
        $collectionId = (int) $request->get('id');

        $user = $this->getUser();

        $manager = $this->getDoctrine()->getManager();

        $collection = $manager->getRepository('AppBundle:Collection')->find($collectionId);

        $correctUser = ($user->getId() === $collection->getAuthor()->getId());

        if(!$correctUser) {
            return $this->redirectToRoute('homepage');
        }

        $collectionService = $this->get('app.collection');

        $username = $request->get('username');

        $otherUser = $manager->getRepository('AppBundle:User')->findOneBy(['username' => $username]);

        if($otherUser->getId() === $user->getId()) {
            return $this->redirectToRoute('homepage');
        }

        if($otherUser) {
            $collectionService->share($collection, $otherUser);
        }

        return $this->redirectToRoute('homepage_collections');
    }

    public function newFormAction(Request $request)
    {
        $user = $this->getUser();

        $nurlService = $this->get('app.nurl');

        $nurlService->get($user->getId());
        $nurlService->all();
        $nurls = $nurlService->done();

        return $this->render('default/collection_create.html.twig', [
            'nurls' => $nurls
        ]);
    }

    public function newAction(Request $request)
    {
        $user = $this->getUser();

        $title = $request->get('title');
        $nurls = $request->get('nurls');

        if(empty($title)) {
            $title = $user->getUsername() . '\'s collection created at ' . microtime();
        }

        $public = $request->get('public');

        if(empty($public)) {
            $public = false;
        }

        if(empty($nurls[0])) {
            $nurls = [];
        }

        $public = $public ? true : false;

        $options = ['title' => $title, 'public' => $public, 'nurls' => $nurls];

        $collectionService = $this->get('app.collection');

        $collectionService->create($user, $options);

        return $this->redirectToRoute('homepage_collections');
    }

    public function addNurlFormAction(Request $request)
    {
        $user = $this->getUser();

        $nurlId = $request->get('id');

        $collectionService = $this->get('app.collection');

        $collectionService->get($user->getId());
        $collectionService->all();
        $collections = $collectionService->done();

        $actionPath = $this->generateUrl('collection_add_nurl', ['id' => $nurlId]);

        return $this->render('default/collection_add_nurl_form.html.twig', [
            'action' => $actionPath,
            'collections' => $collections
        ]);
    }

    public function addNurlAction(Request $request)
    {
        $nurlId = $request->get('id');
        $collectionId = $request->get('collection');

        $user = $this->getUser();

        $collectionService = $this->get('app.collection');

        $collectionService->addNurl($collectionId, $nurlId);

        return $this->redirectToRoute('homepage_collections');
    }

    public function editFormAction(Request $request)
    {
        $collectionId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $collection = $manager->getRepository('AppBundle:Collection')->find($collectionId);

        $collectionService = $this->get('app.collection');

        $nurls = $collectionService->getNurls($collectionId);

        return $this->render('default/collection_edit.html.twig', [
            'collection' => $collection,
            'nurls' => $nurls
        ]);
    }

    public function editAction(Request $request)
    {
        $collectionId = $request->get('id');
        $title = $request->get('title');
        $public = $request->get('public');

        $user = $this->getUser();

        if(empty($public)) {
            $public = false;
        }

        $manager = $this->getDoctrine()->getManager();

        $collection = $manager->getRepository('AppBundle:Collection')->find($collectionId);

        if($collection->getAuthor()->getId() !== $user->getId()) {
            return $this->redirectToRoute('homepage');
        }

        $collectionService = $this->get('app.collection');

        $nurls = $request->get('nurls');

        $options = [
            'title' => $title,
            'public' => $public,
            'removeNurls' => $nurls
        ];

        $collectionService->edit($collection, $options);

        return $this->redirectToRoute('homepage_collections');
    }

    public function makePublicAction(Request $request)
    {
        $user = $this->getUser();

        $collectionId = $request->get('id');
        $userId = $user->getId();

        $manager = $this->getDoctrine()->getManager();

        $collection = $manager->getRepository('AppBundle:Collection');

        if($userId === $collection->getAuthor()->getId()) {
            $this->get('app.collection')->makePublic($collectionId);
        }

        return $this->redirectToRoute('homepage_collections');
    }

}