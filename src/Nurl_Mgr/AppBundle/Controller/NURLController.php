<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 21/03/2017
 * Time: 10:21
 */

namespace Nurl_Mgr\AppBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nurl_Mgr\AppBundle\Entity\NURL;
use Nurl_Mgr\AppBundle\Entity\Tag;
use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\Issue;
use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use \DateTime;

class NURLController extends Controller implements ValidUserController
{

    public function getAction(Request $request)
    {
        $public = $request->get('public');
        $private = $request->get('private');
        $shared = $request->get('shared');
        $search = $request->get('search');
        $searchTags = $request->get('tags');
        $createdFrom = $request->get('created-from');
        $createdTo = $request->get('created-to');
        $editedFrom = $request->get('edited-from');
        $editedTo = $request->get('edited-to');

        $performedSearch = false;

        if(empty($public) && empty($private)) {
            $public = true;
            $private = true;
            $shared = true;
        }

        $userId = null;

        $loggedIn = $this->isGranted('IS_AUTHENTICATED_REMEMBERED');

        if($loggedIn) {
            $userId = $this->getUser()->getId();
        }

        $manager = $this->getDoctrine()->getManager();

        $nurlService = $this->get('app.nurl');

        $nurlService->get($userId);

        // if user logged in, filter and search
        if($loggedIn) {
            if($public) {
                $nurlService->public();
            }
            if($private) {
                $nurlService->private();
            }
            if($shared) {
                $nurlService->shared();
            }

            if (!empty($search) || !empty($createdFrom) || !empty($createdTo) || !empty($editedFrom)
                || !empty($editedTo)) {
                $dates = [
                    'createdFrom' => $createdFrom,
                    'createdTo' => $createdTo,
                    'editedFrom' => $editedFrom,
                    'editedTo' => $editedTo
                ];
                $nurlService->search($search, $dates);
                $performedSearch = true;
            }

        } else {
            $nurlService->public();
        }

        $allNurls = $nurlService->done();

        $tagNurlsRepo = $manager->getRepository('AppBundle:TagNURLs');

        if(!empty($searchTags[0])) {
            $allNurls = array_filter($allNurls, function(NURL $nurl) use($tagNurlsRepo, $searchTags) {
                $hasTags = true;
                foreach($searchTags as $tagId) {
                    $hasTags = $hasTags &&
                        $tagNurlsRepo->findBy((['nurl' => $nurl->getId(), 'tag' => $tagId]));
                }
                return $hasTags;
            });
            $performedSearch = true;
        } else {
            $searchTags = null;
        }

        $nurls = [];

        // get all tags for each nurl
        foreach ($allNurls as $nurl) {
            $nurlId = $nurl->getId();
            $tags = $nurlService->getTags($nurlId, $userId);
            $nurls[] = ['nurl' => $nurl, 'tags' => $tags];
        }

        usort($nurls, function($a, $b) {
            $d1 = $a['nurl']->getCreated();
            $d2 = $b['nurl']->getCreated();
            if($d1 == $d2) {
                return 0;
            }
            return ($d1 > $d2) ? -1 : +1;
        });

        $args = [
            'nurls' => $nurls,
            'active_tab' => 'nurls',
            'public' => $public,
            'private' => $private,
            'shared' => $shared
        ];

        if($performedSearch) {
            $args['search'] = [
                'tags' => $searchTags,
                'text' => $search
            ];
        } else {
            $args['search'] = false;
        }

        return $this->render('default/nurls.html.twig', $args);
    }

    public function searchFormAction(Request $request)
    {
        $title = 'Search for a NURL';
        $actionPath = $this->get('router')->getRouteCollection()->get('homepage_nurls')->getPath();

        $tagService = $this->get('app.tag');

        $allTags = $tagService->getAcceptedTags();

        if($user = $this->getUser()) {
            $personalTags = $tagService->getPersonalTags($user->getId());
            $allTags = array_merge($allTags, $personalTags);
        }

        return $this->render('default/nurl_search.html.twig', [
            'title' => $title,
            'action' => $actionPath,
            'tags' => $allTags,
            'privateName' => 'private',
            'publicName' => 'public',
            'sharedName' => 'shared',
            'searchName' => 'search'
        ]);
    }

    public function submissionFormAction(Request $request)
    {
        $actionPath = $this->generateUrl('nurl_submit');

        $manager = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $tagService = $this->get('app.tag');

        $tags = [];

        $tags = $tagService->getAcceptedTags();

        if($user) {
            $personal = $tagService->getPersonalTags($user->getId());
            $tags = array_merge($tags, $personal);
        }

        return $this->render('default/nurl_form.html.twig', [
            'title' => 'Submit a new nurl',
            'action' => $actionPath,
            'type' => 'create',
            'tags' => $tags
        ]);
    }

    public function submitAction(Request $request)
    {
        $title = $request->get('nurl-title');
        $summary = $request->get('nurl-summary');
        $content = $request->get('nurl-content');
        $nurlTags = $request->get('nurl-tags');

        $created = new \DateTime();

        $manager = $this->getDoctrine()->getManager();

        $tagRepo = $manager->getRepository('AppBundle:Tag');

        $tags = [];

        foreach($nurlTags as $tag) {
            $tags[] = $tagRepo->find($tag);
        }

        $nurlData = [
            'title' => $title,
            'summary' => $summary,
            'content' => $content,
            'public' => true,
            'created' => $created,
            'edited' => $created,
            'tags' => $tags
        ];

        if($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $nurlData['author'] = $this->getUser();
            $nurlData['accepted'] = true;

            $private = $request->get('private');
            if($private) {
                $nurlData['public'] = false;
            }
        }

        $nurlService = $this->get('app.nurl');

        $nurlService->create($nurlData);

        return $this->redirectToRoute('homepage_nurls');
    }

    public function issueFormAction(Request $request)
    {
        $id = $request->get('id');
        return $this->render('default/nurl_issue.html.twig', ['id' => $id]);
    }

    public function issueAction(Request $request)
    {
        $id = $request->get('id');
        $description = $request->get('issue-description');
        $email = $request->get('issue-email');
        $date = new \DateTime();

        if(strlen($email) === 0) {
            $email = null;
        }

        $manager = $this->getDoctrine()->getManager();

        $nurl = $manager->getRepository('AppBundle:NURL')->find($id);

        $issue = new Issue();
        $issue->setDescription($description);
        $issue->setNurl($nurl);
        $issue->setTimeStamp($date);
        $issue->setReporterEmail($email);

        $manager->persist($issue);

        $manager->flush();

        $nurlService = $this->get('app.nurl');

        $nurlService->freeze($nurl->getId());

        return $this->redirectToRoute('homepage');
    }

    public function makePublicAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $nurlId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();
        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        if($userId === $nurl->getAuthor()->getId()) {
            $nurlService = $this->get('app.nurl');
            $nurlService->makePublic($nurl);
        }

        return $this->redirectToRoute('homepage_nurls');
    }

    public function deleteAction(Request $request)
    {
        $nurlId = (int) $request->get('id');

        $nurlService = $this->get('app.nurl');
        $nurlService->delete($nurlId);

        return $this->redirectToRoute('homepage_nurls');
    }

    public function editFormAction(Request $request)
    {
        $nurlId = (int) $request->get('id');

        $manager = $this->getDoctrine()->getManager();
        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        $tagNurlRepo = $manager->getRepository('AppBundle:TagNURLs');

        $tagNurls = $tagNurlRepo->findBy(['nurl' => $nurl->getId()]);

        $editTags = [];

        $tagService = $this->get('app.tag');

        $newTags = array_merge($tagService->getPersonalTags($this->getUser()->getId()),
            $tagService->getAcceptedTags());

        foreach($tagNurls as $tagNurl) {
            $editTags[] = $tagNurl->getTag();
            $newTags = array_filter($newTags, function($tag) use ($tagNurl) {
                return $tag->getId() !== $tagNurl->getTag()->getId();
            });
        }

        $actionPath = $this->generateUrl('nurl_edit', [
            'id' => $nurl->getId()
        ]);

        return $this->render('default/nurl_form.html.twig', [
            'nurl' => $nurl,
            'title' => 'Edit a NURL',
            'action' => $actionPath,
            'type' => 'edit',
            'edit_tags' => $editTags,
            'new_tags' => $newTags
        ]);
    }

    public function editAction(Request $request)
    {
        $nurlId = (int) $request->get('id');
        $editTags = $request->get('edit-tags');
        $newTags = $request->get('new-tags');

        if(!$this->getUser()) {
            return $this->redirectToRoute('homepage');
        }

        $user = $this->getUser();

        $manager = $this->getDoctrine()->getManager();

        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        $tagNurls = $manager->getRepository('AppBundle:TagNURLs')->findBy(['nurl' => $nurlId]);

        $deleteTagNurls = [];

        foreach($tagNurls as $tagNurl) {
            $tag = $tagNurl->getTag();
            if(!(in_array($tag->getId(), $editTags))) {
                $deleteTagNurls[] = $tagNurl;
            }
        }

        foreach($deleteTagNurls as $tagNurl) {
            $manager->remove($tagNurl);
        }

        $tagRepo = $manager->getRepository('AppBundle:Tag');

        foreach($newTags as $newTag) {
            $tag = $tagRepo->find($newTag);

            $tagNurl = new TagNURLs();
            $tagNurl->setNurl($nurl);
            $tagNurl->setTag($tag);

            $manager->persist($tagNurl);
        }

        if($nurl->getAuthor()->getId() !== $user->getId()) {
            return $this->redirectToRoute('homepage');
        }

        $nurlService = $this->get('app.nurl');

        $info = [
            'title' => $request->get('nurl-title'),
            'summary' => $request->get('nurl-summary'),
            'content' => $request->get('nurl-content')
        ];

        $nurlService->edit($nurl, $info);

        $manager->flush();

        return $this->redirectToRoute('homepage');
    }

    public function shareFormAction(Request $request)
    {
        $nurlId = (int) $request->get('id');

        $actionPath = $this->generateUrl('nurl_share', [
            'id' => $nurlId
        ]);

        return $this->render('default/entity_share.html.twig', [
            'entity' => 'NURL',
            'action' => $actionPath
        ]);
    }

    public function shareAction(Request $request)
    {
        $nurlId = (int) $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        $correctUser = ($user->getId() === $nurl->getAuthor()->getId());
        $privateNurl = (!$nurl->getIsPublic());

        if(!$correctUser || !$privateNurl) {
            return $this->redirectToRoute('homepage');
        }

        $nurlService = $this->get('app.nurl');

        $username = $request->get('username');

        $otherUser = $manager->getRepository('AppBundle:User')->findOneBy(['username' => $username]);

        if($otherUser->getId() === $user->getId()) {
            return $this->redirectToRoute('homepage');
        }

        if($otherUser) {
            $nurlService->share($nurl, $otherUser);
        }

        return $this->redirectToRoute('homepage');
    }

    public function viewNurlAction(Request $request)
    {
        $nurlId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);

        $tagNurls = $manager->getRepository('AppBundle:TagNURLs');

        $tags = [];

        foreach($tagNurls as $tagNurl) {
            $tags[] = $tagNurl->getTag();
        }

        $args = ['entry' => ['nurl' => $nurl, 'tags' => $tags   ]];

        return $this->render('default/view_nurl.html.twig', $args);
    }

}