<?php

namespace Nurl_Mgr\AppBundle\Controller;

use Nurl_Mgr\AppBundle\Entity\User;
use Nurl_Mgr\AppBundle\Form\CreateUserFormType;
use Nurl_Mgr\AppBundle\Form\EditUserFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nurl_Mgr\AppBundle\Entity\NURL;
use Nurl_Mgr\AppBundle\Entity\Tag;
use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use Nurl_Mgr\AppBundle\Entity\TagVotes;

class AdminController extends Controller
{

    public function indexAction(Request $request)
    {
        return $this->render('admin/index.html.twig');
    }

    public function selectUserAction(Request $request)
    {
        $userId = $this->getUser()->getId();

        $manager = $this->getDoctrine()->getManager();

        $queryBuilder = $manager->getRepository('AppBundle:User')->createQueryBuilder('u');

        $queryBuilder->where('u.id != :adminId')
            ->setParameter('adminId', $userId);

        $users = $queryBuilder->getQuery()->getResult();

        return $this->render('admin/select_user.html.twig', [
            'users' => $users
        ]);
    }

    public function createUserFormAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(CreateUserFormType::class, $user, [
            'action' => $this->generateUrl('admin_create_user')
        ]);
        $form->get('userType')->setData('normal');
        $view = $form->createView();
        return $this->render('admin/user_form.html.twig', [
            'form' => $view
        ]);
    }

    public function createUserAction(Request $request)
    {
        $form = $this->createForm(CreateUserFormType::class);
        $form->handleRequest($request);
        $userType = $form->get('userType')->getData();
        $user = $form->getData();
        if($userType === 'admin') {
            $user->addRole('ROLE_ADMIN');
        } else if($userType === 'moderator') {
            $user->addRole('ROLE_MODERATOR');
        } else {
            $user->addRole('ROLE_USER');
        }
        $user->setUpdated(new \DateTime());
        $user->setEnabled(true);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();
        return $this->redirectToRoute('admin_page');
    }

    public function editUserFormAction(Request $request)
    {
        $userId = $request->get('id');
        $manager = $this->getDoctrine()->getManager();
        $user = $manager->getRepository('AppBundle:User')->find($userId);
        if(!$user) {
            return $this->redirect('admin_edit_user_form');
        }

        $role = 'normal';

        if($user->hasRole('ROLE_MODERATOR')) {
            $role = 'moderator';
        } else if($user->hasRole('ROLE_ADMIN')) {
            $role = 'admin';
        }

        $form = $this->createForm(EditUserFormType::class, $user, [
            'action' => $this->generateUrl('admin_edit_user', ['id' => $userId])
        ]);

        $form->get('userType')->setdata($role);

        $view = $form->createView();
        return $this->render('admin/user_form.html.twig', [
            'form' => $view
        ]);
    }

    public function editUserAction(Request $request)
    {
        $userId = $request->get('id');
        $form = $this->createForm(EditUserFormType::class);
        $form->handleRequest($request);
        $userType = $form->get('userType')->getData();
        $submittedUser = $form->getData();

        $manager = $this->getDoctrine()->getManager();
        $user = $manager->getRepository('AppBundle:User')->find($userId);

        if($userType === 'admin' && !$user->hasRole('ROLE_ADMIN')) {
            $user->setRoles(['ROLE_ADMIN']);
        } else if($userType === 'moderator' && !$user->hasRole('ROLE_MODERATOR')) {
            $user->setRoles(['ROLE_MODERATOR']);
        } else if(!$user->hasRole('ROLE_USER')) {
            $user->setRoles(['ROLE_USER']);
        }
        $user->setUsername($submittedUser->getUsername());
        $user->setEmail($submittedUser->getEmail());
        $user->setImageFile($submittedUser->getImageFile());

        $user->setUpdated(new \DateTime());

        $manager->persist($user);
        $manager->flush();
        return $this->redirectToRoute('admin_page');
    }

    public function deleteUserAction(Request $request)
    {
        $userId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $this->get('app.user')->delete($userId);

        return $this->redirectToRoute('admin_page');
    }
}
