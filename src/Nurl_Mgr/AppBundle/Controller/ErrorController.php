<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 09/04/2017
 * Time: 06:33
 */

namespace Nurl_Mgr\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ErrorController extends Controller
{
    public function accountFrozenAction(Request $request)
    {
        $user = $this->getUser();
        if(!$user->isFrozen()) {
            return $this->redirectToRoute('homepage');
        }

        $manager = $this->getDoctrine()->getManager();

        $freeze = $manager->getRepository('AppBundle:AccountFreeze')->findBy(['user' => $user])[0];

        return $this->render('default/frozen.html.twig', ['message' => $freeze]);
    }
}