<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 02/04/2017
 * Time: 22:14
 */

namespace Nurl_Mgr\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nurl_Mgr\AppBundle\Entity\NURL;
use Nurl_Mgr\AppBundle\Entity\Tag;
use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use Nurl_Mgr\AppBundle\Entity\TagVotes;
use Nurl_Mgr\AppBundle\Entity\User;

class UserController extends Controller implements ValidUserController
{
    public function deleteFormAction(Request $request)
    {
        return $this->render('default/user_delete.html.twig');
    }

    public function deleteAction(Request $request)
    {
        $user = $this->getUser();

        $encoderFactory = $this->get('security.encoder_factory');

        $encoder = $encoderFactory->getEncoder($user);

        $password = $request->get('password');

        $valid = $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());

        $userService = $this->get('app.user');

        $manager = $this->getDoctrine()->getManager();

        if($valid) {
            $userService->delete($user->getId());
            return $this->redirectToRoute('homepage');
        } else {
            return $this->redirectToRoute('fos_user_profile_show');
        }
    }

    /*
     * public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $manager = $this->getDoctrine()->getManager();

        $nurlService = $this->get('app.nurl');

        $tagService = $this->get('app.tag');

        $userId = $user->getId();

        $nurlService->get($manager, $userId);
        $nurlService->own();
        $ownNurls = $nurlService->done();

        for($i=0; $i<count($ownNurls); $i++) {
            $nurl = $ownNurls[$i];
            $tags = $nurlService->getTags($manager, $nurl->getId(), $userId);
            $ownNurls[$i] = ['nurl' => $nurl, 'tags' => $tags];
        }

        $ownTags = $tagService->getAllOwnTags($manager, $userId);

        return $this->render('@FOSUser/Profile/show.html.twig', [
            'user' => $user,
            'nurls' => $ownNurls,
            'tags' => $ownTags
        ]);
    }
     */

    public function publicProfileAction(Request $request)
    {
        $userId = $request->get('id');

        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($userId);

        $private = $user->getPrivate();

        $args = ['user' => $user, 'nurls' => null, 'tags' => null, 'showContent' => !$private];

        if(!$private) {

            $nurlService = $this->get('app.nurl');

            $tagService = $this->get('app.tag');

            $nurlService->get($userId);
            $nurlService->ownPublic();
            $userNurls = $nurlService->done();

            for ($i = 0; $i < count($userNurls); $i++) {
                $nurl = $userNurls[$i];
                $tags = $nurlService->getTags($nurl->getId(), $userId);
                $userNurls[$i] = ['nurl' => $nurl, 'tags' => $tags];
            }

            $userTags = $tagService->getUserPublicTags($userId);

            $args['nurls'] = $userNurls;
            $args['tags'] = $userTags;
        }

        return $this->render('@FOSUser/Profile/show.html.twig', $args);
    }

    public function changeAccountVisibilityAction(Request $request)
    {
        $id = $request->get('id');

        $user = $this->getUser();

        if($user->getId() != $id) {
            return $this->redirectToRoute('homepage');
        }

        $manager = $this->getDoctrine()->getManager();

        $user->setPrivate(!$user->getPrivate());

        $manager->persist($user);

        $manager->flush();

        return $this->redirectToRoute('fos_user_profile_show');
    }
}
