<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 16/02/2017
 * Time: 23:36
 */

namespace Nurl_Mgr\AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class User
 *
 * A registered user.
 *
 * @package Nurl_Mgr\AppBundle\Entity
 */
class User extends BaseUser
{
    /**
     * Identifier
     * @var integer
     */
    protected $id;

    /**
     * The user's username
     * @var string
     */
    protected $username;

    /**
     * User's email address
     * @var string
     */
    protected $email;

    /**
     * User's password
     * @var string
     */
    protected $password;

    /**
     * User's frozen state. Account can be frozen by moderator.
     * @var boolean
     */
    private $isFrozen = false;

    /**
     * @var string
     */
    private $profilePicture = 'default.jpg';

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var File
     */
    private $imageFile;

    public function __construct()
    {
        parent::__construct();
        $this->roles = ['ROLE_USER'];
        if(empty($this->updated)) {
            $this->updated = new \DateTime();
        }
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if($image) {
            $this->updated = new \DateTime();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return bool
     */
    public function isFrozen()
    {
        return $this->getIsFrozen();
    }


    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return User
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set profilePicture
     *
     * @param string $profilePicture
     *
     * @return User
     */
    public function setProfilePicture($profilePicture)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture
     *
     * @return string
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username.
     *
     * @param string $username The new username.
     *
     * @return User Self with updated username.
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string The username.
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email address.
     *
     * @param string $email The new email address.
     *
     * @return User Self with updated email.
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email address.
     *
     * @return string The user's email address.
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password.
     *
     * @param string $password The new password.
     *
     * @return User Self with updated password.
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string The user's password.
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set frozen state.
     *
     * @param boolean $isFrozen Frozen state.
     *
     * @return User Self with updated frozen state.
     */
    public function setIsFrozen($isFrozen)
    {
        $this->isFrozen = $isFrozen;

        return $this;
    }

    /**
     * Get frozen state.
     *
     * @return boolean The frozen state. True if account if frozen, false otherwise.
     */
    public function getIsFrozen()
    {
        return $this->isFrozen;
    }
    /**
     * @var boolean
     */
    private $private = false;


    /**
     * Set private
     *
     * @param boolean $private
     *
     * @return User
     */
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * Get private
     *
     * @return boolean
     */
    public function getPrivate()
    {
        return $this->private;
    }
}
