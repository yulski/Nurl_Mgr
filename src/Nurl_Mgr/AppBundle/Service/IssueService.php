<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 07/04/2017
 * Time: 18:54
 */

namespace Nurl_Mgr\AppBundle\Service;

use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\CollectionAccess;
use Nurl_Mgr\AppBundle\Entity\CollectionNURLs;
use Nurl_Mgr\AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;

class IssueService
{
    protected $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getMessageArchive()
    {
        $ret = [];

        $messages = $this->manager->getRepository('AppBundle:NURLMessage')->findAll();
        $messageIssueRepo = $this->manager->getRepository('AppBundle:MessageIssues');

        foreach($messages as $message) {
            $curr = ['message' => $message, 'issues' => [], 'nurl' => $message->getNurl()];
            $currMessageIssues = $messageIssueRepo->findBy(['message' => $message->getId()]);
            foreach($currMessageIssues as $currMessageIssue) {
                $currIssue = $currMessageIssue->getIssue();
                $curr['issues'][] = $currIssue;
            }
            $ret[] = $curr;
        }

        return $ret;
    }
}