<?php

namespace Nurl_Mgr\AppBundle\Entity;

/**
 * NURLAccess
 */
class NURLAccess
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\NURL
     */
    private $nurl;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nurl
     *
     * @param \Nurl_Mgr\AppBundle\Entity\NURL $nurl
     *
     * @return NURLAccess
     */
    public function setNurl(\Nurl_Mgr\AppBundle\Entity\NURL $nurl = null)
    {
        $this->nurl = $nurl;

        return $this;
    }

    /**
     * Get nurl
     *
     * @return \Nurl_Mgr\AppBundle\Entity\NURL
     */
    public function getNurl()
    {
        return $this->nurl;
    }

    /**
     * Set user
     *
     * @param \Nurl_Mgr\AppBundle\Entity\User $user
     *
     * @return NURLAccess
     */
    public function setUser(\Nurl_Mgr\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Nurl_Mgr\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
