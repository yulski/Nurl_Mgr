<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 05/04/2017
 * Time: 22:51
 */

namespace Nurl_Mgr\AppBundle\Entity;


class NURLMessage
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $timeStamp;

    /**
     * @var \Nurl_Mgr\AppBundle\Entity\NURL
     */
    private $nurl;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $issue;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->issue = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return NURLMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set timeStamp
     *
     * @param \DateTime $timeStamp
     *
     * @return NURLMessage
     */
    public function setTimeStamp($timeStamp)
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    /**
     * Get timeStamp
     *
     * @return \DateTime
     */
    public function getTimeStamp()
    {
        return $this->timeStamp;
    }

    /**
     * Set nurl
     *
     * @param \Nurl_Mgr\AppBundle\Entity\NURL $nurl
     *
     * @return NURLMessage
     */
    public function setNurl(\Nurl_Mgr\AppBundle\Entity\NURL $nurl = null)
    {
        $this->nurl = $nurl;

        return $this;
    }

    /**
     * Get nurl
     *
     * @return \Nurl_Mgr\AppBundle\Entity\NURL
     */
    public function getNurl()
    {
        return $this->nurl;
    }

    /**
     * Add issue
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Issue $issue
     *
     * @return NURLMessage
     */
    public function addIssue(\Nurl_Mgr\AppBundle\Entity\Issue $issue)
    {
        $this->issue[] = $issue;

        return $this;
    }

    /**
     * Remove issue
     *
     * @param \Nurl_Mgr\AppBundle\Entity\Issue $issue
     */
    public function removeIssue(\Nurl_Mgr\AppBundle\Entity\Issue $issue)
    {
        $this->issue->removeElement($issue);
    }

    /**
     * Get issue
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIssue()
    {
        return $this->issue;
    }
    /**
     * @var boolean
     */
    private $deleted;


    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return NURLMessage
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
