<?php

namespace Nurl_Mgr\AppBundle\Service;

use Nurl_Mgr\AppBundle\Entity\Tag;
use Nurl_Mgr\AppBundle\Entity\TagVotes;
use Nurl_Mgr\AppBundle\Entity\User;
use Doctrine\DBAL\Query\QueryBuilder;
use \Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Nurl_Mgr\AppBundle\Entity\NURL;

class TagService
{
    protected $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getAcceptedTags()
    {
        return $this->manager->getRepository('AppBundle:Tag')->findBy(
            ['isAccepted' => true, 'isPublic' => true]);
    }

    public function getProposedTags()
    {
        return $this->manager->getRepository('AppBundle:Tag')->findBy(['isAccepted' => false]);
    }

    public function getPersonalTags($userId)
    {
        return $this->manager->getRepository('AppBundle:Tag')->findBy([
            'isPublic' => false, 'author' => $userId, 'isAccepted' => true
        ]);
    }

    public function getAllOwnTags($userId)
    {
        return $this->manager->getRepository('AppBundle:Tag')->findBy([
            'author' => $userId
        ]);
    }

    public function getUserPublicTags($userId)
    {
        return $this->manager->getRepository('AppBundle:Tag')->findBy([
            'author' => $userId,
            'isPublic' => true
        ]);
    }

    public function getAll($userId)
    {
        $accepted = $this->getAcceptedTags();
        $proposed = $this->getProposedTags();
        $arr = ['accepted' => $accepted, 'proposed' => $proposed];
        if($userId) {
            $personal = $this->getPersonalTags($userId);
            $arr['personal'] = $personal;
        }
        return $arr;
    }

    public function upVote($tagId, User $user = null, $ipAddress = null)
    {
        if ($user) {
            $tagVotes = $this->manager->getRepository('AppBundle:TagVotes')
                ->findBy(['user' => $user->getId(), 'tag' => $tagId]);
            if (count($tagVotes) === 0) {
                $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                $userVote = new TagVotes();
                $userVote->setUser($user);
                $userVote->setTag($tag);
                $userVote->setVote(5);

                $this->manager->persist($userVote);
                $tag->incrementVotes(5);
            } else {
                $userVote = $tagVotes[0];
                $previousVote = $userVote->getVote();
                $userVote->setVote(5);
                if ($previousVote === -5) {
                    $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                    $tag->incrementVotes(10);
                }
            }
        } else {
            $tagVotes = $this->manager->getRepository('AppBundle:TagVotes')
                ->findBy(['ip_address' => $ipAddress, 'tag' => $tagId]);
            if (count($tagVotes) === 0) {
                $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                $userVote = new TagVotes();
                $userVote->setIpAddress($ipAddress);
                $userVote->setTag($tag);
                $userVote->setVote(1);

                $this->manager->persist($userVote);
                $tag->incrementVotes(1);
            } else {
                $userVote = $tagVotes[0];
                $previousVote = $userVote->getVote();
                $userVote->setVote(1);
                if ($previousVote === -1) {
                    $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                    $tag->incrementVotes(2);
                    echo 'xxxxxxxxxxxxxxxxxxxxxxxx';
                } else {
                    echo 'yyyyyyyyyyyyyy';
                }
            }
        }

        $this->manager->flush();
    }

    public function downVote($tagId, User $user = null, $ipAddress = null)
    {
        if ($user) {
            $tagVotes = $this->manager->getRepository('AppBundle:TagVotes')
                ->findBy(['user' => $user->getId(), 'tag' => $tagId]);
            if (count($tagVotes) === 0) {
                $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                $userVote = new TagVotes();
                $userVote->setUser($user);
                $userVote->setTag($tag);
                $userVote->setVote(-5);

                $this->manager->persist($userVote);
                $tag->decrementVotes(5);
            } else {
                $userVote = $tagVotes[0];
                $previousVote = $userVote->getVote();
                $userVote->setVote(-5);
                if ($previousVote === 5) {
                    $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                    $tag->decrementVotes(10);
                }
            }
        } else {
            $tagVotes = $this->manager->getRepository('AppBundle:TagVotes')
                ->findBy(['ip_address' => $ipAddress, 'tag' => $tagId]);
            if (count($tagVotes) === 0) {
                $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                $userVote = new TagVotes();
                $userVote->setIpAddress($ipAddress);
                $userVote->setTag($tag);
                $userVote->setVote(-1);

                $this->manager->persist($userVote);
                $tag->decrementVotes(1);
            } else {
                $userVote = $tagVotes[0];
                $previousVote = $userVote->getVote();
                $userVote->setVote(-1);
                if ($previousVote === 1) {
                    $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
                    $tag->decrementVotes(2);
                }
            }
        }

        $this->manager->flush();
    }

    public function create(array $options)
    {
        $tag = new Tag();
        $tag->setIsPublic($options['public']);
        $tag->setAuthor($options['author']);
        $tag->setValue($options['content']);
        $tag->setIsAccepted($options['accepted']);
        $this->manager->persist($tag);
        $this->manager->flush();
    }

    public function edit(Tag $tag, $options)
    {
        $tag->setValue($options['value']);
        $tag->setIsPublic($options['public']);
        $this->manager->persist($tag);
        $this->manager->flush();
    }

    public function delete($tagId)
    {
        $tag = $this->manager->getRepository('AppBundle:Tag')->find($tagId);
        $this->manager->remove($tag);
        $this->manager->flush();
    }
}
