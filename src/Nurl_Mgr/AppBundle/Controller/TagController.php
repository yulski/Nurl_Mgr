<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/03/2017
 * Time: 19:06
 */

namespace Nurl_Mgr\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nurl_Mgr\AppBundle\Entity\NURL;
use Nurl_Mgr\AppBundle\Entity\Tag;
use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\TagNURLs;
use Nurl_Mgr\AppBundle\Entity\TagVotes;

class TagController extends Controller implements ValidUserController
{
    public function getAction(Request $request)
    {
        $tagService = $this->get('app.tag');

        $userId = false;

        if($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $userId = $this->getUser()->getId();
        }

        $tags = $tagService->getAll($userId);

        $args = [
            'accepted_tags' => $tags['accepted'],
            'proposed_tags' => $tags['proposed'],
            'active_tab' => 'tags'
        ];

        if(array_key_exists('personal', $tags)) {
            $args['personal_tags'] = $tags['personal'];
        }

        return $this->render('default/tags.html.twig', $args);
    }

    public function upVoteAction(Request $request)
    {
        $tagId = (int) $request->get('id');
        $loggedIn = $this->isGranted('IS_AUTHENTICATED_REMEMBERED');

        $tagService = $this->get('app.tag');

        if($loggedIn) {
            $user = $this->getUser();
            $tagService->upVote($tagId, $user);
        } else {
            $ipAddress = $this->get('request_stack')->getMasterRequest()->getClientIp();
            $tagService->upVote($tagId, null, $ipAddress);
        }

        return $this->redirectToRoute('homepage_tags');
    }

    public function downVoteAction(Request $request)
    {
        $tagId = (int) $request->get('id');
        $loggedIn = $this->isGranted('IS_AUTHENTICATED_REMEMBERED');

        $tagService = $this->get('app.tag');

        if($loggedIn) {
            $user = $this->getUser();
            $tagService->downVote($tagId, $user);
        } else {
            $ipAddress = $this->get('request_stack')->getMasterRequest()->getClientIp();
            $tagService->downVote($tagId, null, $ipAddress);
        }

        return $this->redirectToRoute('homepage_tags');
    }

    public function submitAction(Request $request)
    {
        $content = $request->get('new-tag');
        $public = false;

        $user = $this->getUser();

        if($user && $public) {
            $public = true;
        }

        $manager = $this->getDoctrine()->getManager();

        $options = [
            'content' => $content,
            'public' => $public,
            'author' => $user,
            'accepted' => ($user != null) ? true : false
        ];

        $tagService = $this->get('app.tag');

        $tagService->create($options);

        return $this->redirectToRoute('homepage_tags');
    }

    public function editFormAction(Request $request)
    {
        $tagId = $request->get('id');

        $tag = $this->getDoctrine()->getRepository('AppBundle:Tag')->find($tagId);

        return $this->render('default/tag_edit.html.twig', [
            'tag' => $tag
        ]);
    }

    public function editAction(Request $request)
    {
        $tagId = $request->get('id');

        if(!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('homepage');
        }

        $value = $request->get('tag-content');
        $public = $request->get('tag-public');

        $manager = $this->getDoctrine()->getManager();

        $tag = $manager->getRepository('AppBundle:Tag')->find($tagId);

        if($tag->getAuthor()->getId() !== $this->getUser()->getId() || $tag->getIsPublic() === true) {
            return $this->redirectToRoute('homepage');
        }

        $tagService = $this->get('app.tag');

        $options = [
            'value' => $value,
            'public' => $public ? true : false
        ];

        $tagService->edit($tag, $options);

        return $this->redirectToRoute('homepage_tags');
    }

    public function deleteAction(Request $request)
    {
        $tagId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $tag = $manager->getRepository('AppBundle:Tag')->find($tagId);

        $user = $this->getUser();

        if($user->getId() !== $tag->getAuthor()->getId() || $tag->getIsPublic() == true) {
            return $this->redirectToRoute('homepage');
        }

        $tagService = $this->get('app.tag');

        $tagService->delete($tagId);

        return $this->redirectToRoute('homepage_tags');
    }

    public function addNurlFormAction(Request $request)
    {
        $tagId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();

        $tag = $manager->getRepository('AppBundle:Tag')->find($tagId);

        $nurlService = $this->get('app.nurl');

        $nurlService->get($this->getUser()->getId());
        $nurlService->own();
        $nurls = $nurlService->done();

        return $this->render('default/tag_add_nurl.html.twig', ['tag' => $tag, 'nurls' => $nurls]);
    }

    public function addNurlAction(Request $request)
    {
        $tagId = $request->get('id');
        $nurlId = $request->get('nurl');

        $manager = $this->getDoctrine()->getManager();

        $nurl = $manager->getRepository('AppBundle:NURL')->find($nurlId);
        $tag = $manager->getRepository('AppBundle:Tag')->find($tagId);

        $tagNurl = new TagNURLs();
        $tagNurl->setNurl($nurl);
        $tagNurl->setTag($tag);

        $manager->persist($tagNurl);

        $manager->flush();

        return $this->redirectToRoute('homepage_tags');
    }
}
