<?php


class NURLCest
{
    protected function login(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField(['name' => '_username'], 'some_user');
        $I->fillField(['name' => '_password'], 'some_pas55word');
        $I->click('_submit');
    }

    public function viewTest(AcceptanceTester $I)
    {
        $I->am('user');
        $I->wantTo('view nurls');
        $I->amOnPage('/');
        $I->see('NURLs');
        $I->seeElement('.nurl');
    }

    /**
     * @before login
     */
    public function searchByTextTest(AcceptanceTester $I)
    {
        $I->am('logged in user');
        $I->wantTo('search for a nurl by text');
        $I->amOnPage('/nurl-search');
        $I->fillField(['id' => 'search'], 'title');
        $I->click('Submit');
        $I->seeCurrentUrlMatches('/^\/[^\/]*\/?$/');
        $I->seeElement('.nurl');
        $I->see('title');
    }

    /**
     * @before login
     */
    public function searchByDateTest(AcceptanceTester $I)
    {
        $I->am('logged in user');
        $I->wantTo('search for a nurl by date range');
        $I->amOnPage('/nurl-search');
        $I->fillField(['id' => 'created-from'], '2017-03-05');
        $I->fillField(['id' => 'created-to'], '2017-04-01');
        $I->click('Submit');
        $I->seeCurrentUrlMatches('/^\/[^\/]*\/?$/');
        $I->seeElement('.nurl');
        $I->see('This is a title');
    }

    /**
     * @before login
     */
    public function searchByTagTest(AcceptanceTester $I)
    {
        $I->am('logged in user');
        $I->wantTo('search for a nurl by tag');
        $I->amOnPage('/nurl-search');
        $I->selectOption(['id' => 'tags'], ['example', 'another']);
        $I->click('Submit');
        $I->seeCurrentUrlMatches('/^\/[^\/]*\/?$/');
        $I->seeElement('.nurl');
        $I->see('example');
        $I->see('another');
        $I->see('This is a title');
    }

    public function searchByTagFromTagPageTest(AcceptanceTester $I)
    {
        $I->am('logged in user');
        $I->wantTo('search for a nurl by tag by clicking on a tag');
        $I->amOnPage('/tags');
        $I->click('example');
        $I->seeCurrentUrlMatches('/^\/[^\/]*\/?$/');
        $I->seeElement('.nurl');
        $I->see('example');
        $I->see('This is a title');
    }

    public function submissionFormTest(AcceptanceTester $I)
    {
        $I->am('anonymous user');
        $I->wantTo('view nurl submission form');
        $I->amOnPage('/');
        $I->click('Submit new NURL');
        $I->seeCurrentUrlEquals('/submit-nurl');
        $I->see('Submit a new nurl');
        $I->seeElement('#nurl-title');
        $I->seeElement('#nurl-summary');
        $I->seeElement('#nurl-content');
        $I->see('Submit');
    }
}
