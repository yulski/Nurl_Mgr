<?php

namespace Nurl_Mgr\AppBundle\Service;


use Nurl_Mgr\AppBundle\Entity\Collection;
use Nurl_Mgr\AppBundle\Entity\CollectionAccess;
use Nurl_Mgr\AppBundle\Entity\CollectionNURLs;
use Nurl_Mgr\AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;

class CollectionService
{
    private $buffer;
    private $userId;

    protected $manager;

    public function __construct(EntityManager $manager)
    {
        $this->reset();
        $this->manager = $manager;
    }

    private function reset()
    {
        $this->buffer = [];
        $this->userId = -1;
    }

    public function get($userId)
    {
        $this->userId = $userId;
    }

    public function own()
    {
        $this->buffer[] = $this->manager->getRepository('AppBundle:Collection')->findBy(['author' => $this->userId]);
    }

    public function shared()
    {
        $collectionAccess = $this->manager->getRepository('AppBundle:CollectionAccess')->findBy(['user' => $this->userId]);

        $sharedCollections = [];

        foreach($collectionAccess as $access) {
            $sharedCollections[] = $access->getCollection();
        }

        $this->buffer[] = $sharedCollections;
    }

    public function public()
    {
        $publicCollections = $this->manager->getRepository('AppBundle:Collection')->findBy(['isPublic' => true]);
        $this->buffer[] = $publicCollections;
    }

    public function all() {
        $this->own();
        $this->shared();
        $this->public();
    }

    public function done()
    {
        $result = [];
        foreach($this->buffer as $buffer) {
            $result = array_merge($result, $buffer);
        }
        $this->reset();
        return $result;
    }

    public function getNurls($collectionId)
    {
        $collectionNurlRepository = $this->manager->getRepository('AppBundle:CollectionNURLs');
        $collectionNurls = $collectionNurlRepository->findBy(['collection' => $collectionId]);
        $nurls = [];
        foreach($collectionNurls as $collectionNurl) {
            $nurls[] = $collectionNurl->getNurl();
        }
        return $nurls;
    }

    public function delete($collectionId)
    {
        $collection = $this->manager->getRepository('AppBundle:Collection')->find($collectionId);
        $this->manager->remove($collection);
        $this->manager->flush();
    }

    public function share(Collection $collection, User $user)
    {
        $collectionAccessRepo = $this->manager->getRepository('AppBundle:CollectionAccess');

        $currAccess = $collectionAccessRepo->findBy(['collection' => $collection->getId(), 'user' => $user->getId()]);

        if(count($currAccess) > 0) {
            return;
        }

        $access = new CollectionAccess();
        $access->setUser($user);
        $access->setCollection($collection);
        $this->manager->persist($access);
        $this->manager->flush();
    }

    public function create(User $user, array $options)
    {
        $collection = new Collection();
        $collection->setTitle($options['title']);
        $collection->setIsPublic($options['public']);
        $collection->setAuthor($user);

        $this->manager->persist($collection);
        $this->manager->flush();

        $nurlRepo = $this->manager->getRepository('AppBundle:NURL');

        if(!empty($options['nurls'])) {
            foreach($options['nurls'] as $nurlId) {
                $nurl = $nurlRepo->find($nurlId);
                $collectionNurl = new CollectionNURLs();
                $collectionNurl->setNurl($nurl);
                $collectionNurl->setCollection($collection);
                $this->manager->persist($collectionNurl);
            }
            $this->manager->flush();
        }
    }

    public function addNurl($collectionId, $nurlId)
    {
        $collection = $this->manager->getRepository('AppBundle:Collection')->find($collectionId);

        $nurl = $this->manager->getRepository('AppBundle:NURL')->find($nurlId);

        $collectionNurlRepo = $this->manager->getRepository('AppBundle:CollectionNURLs');

        $curr = $collectionNurlRepo->findBy(['collection' => $collectionId, 'nurl' => $nurlId]);

        if(count($curr) > 0) {
            return;
        }

        $collectionNurl = new CollectionNURLs();
        $collectionNurl->setCollection($collection);
        $collectionNurl->setNurl($nurl);
        $this->manager->persist($collectionNurl);
        $this->manager->flush();
    }

    public function edit(Collection $collection, array $options)
    {
        $collectionNurlsRepo = $this->manager->getRepository('AppBundle:CollectionNURLs');
        $collection->setTitle($options['title']);
        $collection->setIsPublic($options['public']);
        foreach($options['removeNurls'] as $nurlId) {
            $collectionNurl = $collectionNurlsRepo->findOneBy([
                'collection' => $collection->getId(),
                'nurl' => (int) $nurlId
            ]);
            $this->manager->remove($collectionNurl);
        }
        $this->manager->flush();
    }

    public function makePublic($collectionId)
    {
        $collection = $this->manager->getRepository('AppBundle:Collection')->find($collectionId);
        $collection->setIsPublic(true);
        $this->manager->persist($collection);
        $this->manager->flush();
    }
}